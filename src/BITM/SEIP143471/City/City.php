<?php

namespace App\City;
use App\Message\Message;
    use App\Model\Database as DB;
    use App\Utility\Utility;
    use PDO;

class City extends DB
{
    public $id = "";
    public $name = "";
    public $city = "";

    public function __construct()
    {
        parent::__construct();
    }

    public function setData($postVariableData = NULL)
    {

        if (array_key_exists('id', $postVariableData)) {
            $this->id = $postVariableData['id'];
        }

        if (array_key_exists('name', $postVariableData)) {
            $this->name = $postVariableData['name'];
        }

        if (array_key_exists('city', $postVariableData)) {
            $this->city = $postVariableData['city'];
        }


    }

    public function store()
    {
        $arrData = array($this->name, $this->city);
        $sql = "Insert INTO city(name,city) VALUES (?,?)";
        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);

        if ($result)
            Message::setMessage("Success! Data has been inserted successfully! :) ");
        else
            Message::setMessage("Failed! Data has not been inserted successfully! :( ");
        Utility::redirect('create.php');
    }
}


/* public function index()
 {
     echo "index city";
 }


}*/
