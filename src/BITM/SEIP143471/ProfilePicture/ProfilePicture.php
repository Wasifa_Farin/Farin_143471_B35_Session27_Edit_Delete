<?php
namespace App\ProfilePicture;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;

class ProfilePicture extends DB
{
    public $id;
    public $name;
    public $profile_picture;


    public function __construct()
    {

        parent::__construct();
    }


    public function setData($data = NULL)
    {
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('name', $data)) {
            $this->name = $data['name'];
        }
        if (array_key_exists('profile_picture', $data)) {
            $this->profile_picture = $data['profile_picture']['name'];
        }


    }
    /*public function setProfilePictureData($data=NULL)
    {
        if (array_key_exists('profile_picture', $data)) {
            $this->profile_picture = $data['profile_picture']['name'];
        }
    }*/

    public function store()
    {
        $path='C:/xampp/htdocs/Farin_143471_B35_Session26_Views/resource/image/';
        $uploadedFile = $path.basename($this->profile_picture);
        move_uploaded_file($_FILES['profile_picture']['tmp_name'], $uploadedFile);


        $arrData = array($this->name, $this->profile_picture);

        $sql = "insert into profile_picture(name, profile_picture) VALUES (?,?)";
       // $sql = "insert into profile_picture(name, profile_picture) VALUES ($this->name, $this->profile_picture)";
//echo $sql;
       // die;
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrData);
        if ($result)
            Message::setMessage("Success! Data has been inserted successfully!");
        else
            Message::setMessage("Failed! Data has not been inserted successfully");

        Utility::redirect('create.php');

    }

}
